<?php

namespace App\Classes\DayFour;

use Illuminate\Support\Collection;

class Board
{
    private Collection $rows;

    private const BINGO = 'XXXXX';
    private array $chosenNumbers = [];

    public static function make(Collection $rows)
    {
        return new static($rows);
    }

    public function __construct(Collection $rows)
    {
        $this->rows = $rows;
        $this->columns();
    }

    public function markChosen(int $chosenNumber): static
    {
        if (in_array($chosenNumber, $this->chosenNumbers)) {
            return $this;
        }

        $this->rows->transform(function (Collection $row) use ($chosenNumber) {
            $row->transform(function ($value) use ($chosenNumber) {
                return (int) $value === $chosenNumber ? 'X' : $value;
            });

            return $row;
        });

        $this->chosenNumbers[] = $chosenNumber;

        return $this;
    }

    public function bingo(): bool
    {
        return $this->bingoInSet($this->rows) || $this->bingoInSet($this->columns());
    }

    public function bingoInSet(Collection $set): bool
    {
        return $set->filter(fn (Collection $set) => $set->implode('', $set) === static::BINGO)->isNotEmpty();
    }

    public function columns(): Collection
    {
        $columns = [];

        foreach ($this->rows as $rowIndex => $row) {
            foreach ($row as $column => $value) {
                $columns[$column][$rowIndex] = $value;
            }
        }

        return collect($columns)->transform(function ($values) {
            return collect($values);
        });
    }

    public function lastChosenNumber(): ?int
    {
        return $this->chosenNumbers[count($this->chosenNumbers) - 1] ?? null;
    }

    public function getUnmarkedNumbers(): Collection
    {
        $unmarkedNumbers = [];

        $this->rows->each(function ($row) use (&$unmarkedNumbers) {
            $unmarkedNumbers = array_merge($unmarkedNumbers, $row->filter(fn ($value) => $value !== 'X')->toArray());
        });

        return collect($unmarkedNumbers);
    }

    public function toString()
    {
        foreach ($this->rows as $row) {
            foreach ($row as $value) {
                echo str_pad($value, 2, ' ', STR_PAD_LEFT) . " ";
            }
            echo "\n";
        }
    }
}
