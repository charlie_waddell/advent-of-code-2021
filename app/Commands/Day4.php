<?php

namespace App\Commands;

use App\Classes\DayFour\Board;
use App\Traits\ReadsInput;
use Illuminate\Support\Collection;
use LaravelZero\Framework\Commands\Command;

class Day4 extends Command implements Contracts\ReadsInput
{
    use ReadsInput;

    protected $signature = 'day:four';
    protected $description = 'Day four of advent of code';

    private ?Collection $chosenNumbers = null;
    private ?Collection $boards = null;

    public function handle(): void
    {
        $this->info("Part one: {$this->partOne()}");
        $this->info("Part two: {$this->partTwo()}");
    }

    private function partOne(): ?int
    {
        if (! $board = $this->getFirstBoardWithBingo()) {
            $this->warn('No bingo found.');

            return null;
        }

        return $board->getUnmarkedNumbers()->sum() * $board->lastChosenNumber();
    }

    private function partTwo(): ?int
    {
        if (! $board = $this->getLastBoardWithBingo()) {
            $this->warn('No bingo found.');

            return null;
        }

        return $board->getUnmarkedNumbers()->sum() * $board->lastChosenNumber();
    }

    private function getFirstBoardWithBingo(): Board|bool
    {
        foreach ($this->chosenNumbers() as $chosenNumber) {
            foreach ($this->boards() as $board) {
                $board->markChosen($chosenNumber);

                if ($board->bingo()) {
                    return $board;
                }
            }
        }

        return false;
    }

    private function getLastBoardWithBingo(): Board|bool
    {
        $numBoards = $this->boards()->count();
        $numBingo = $this->boards()->filter(fn (Board $board) => $board->bingo())->count();

        foreach ($this->chosenNumbers() as $chosenNumber) {
            $boards = $this->boards()->filter(fn (Board $board) => ! $board->bingo());
            foreach ($boards as $board) {
                $board->markChosen($chosenNumber);
                if ($board->bingo()) {
                    $numBingo++;
                }

                if ($numBoards === $numBingo) {
                    return $board;
                }
            }
        }

        return false;
    }

    private function chosenNumbers(): Collection
    {
        if ($this->chosenNumbers) {
            return $this->chosenNumbers;
        }

        return $this->chosenNumbers = collect(explode(',', $this->getInput()->first()));
    }

    private function boards(): Collection
    {
        if ($this->boards) {
            return $this->boards;
        }

        return $this->boards = $this->getInput()
            ->skip(1)
            ->map(fn ($row) => collect(explode(' ', trim(str_replace('  ', ' ', $row)))))
            ->chunk(5)
            ->map(fn ($rows) => Board::make($rows));
    }

    /**
     * @return string
     */
    public function getFilepath(): string
    {
        return base_path('assets' . DIRECTORY_SEPARATOR . 'day-four' . DIRECTORY_SEPARATOR . 'input.txt');
    }
}
