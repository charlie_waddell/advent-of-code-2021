<?php

namespace App\Commands;

use App\Traits\ReadsInput;
use Illuminate\Support\Collection;
use LaravelZero\Framework\Commands\Command;

class Day7 extends Command implements Contracts\ReadsInput
{
    use ReadsInput{
        getInput as readsInputGetInput;
    }

    protected $signature = 'day:seven';
    protected $description = 'Day seven of advent of code';

    public function handle(): void
    {
        $this->info("Part one: {$this->partOne()}");
        $this->info("Part two: {$this->partTwo()}");
    }

    private function partOne(): int
    {
        $positions = [];

        for ($i = $this->min(); $i <= $this->max(); $i++) {
            if (! array_key_exists($i, $positions)) {
                $positions[$i] = 0;
            }
            $this->getInput()->each(function ($pos) use (&$positions, $i) {
                $positions[$i] += abs($i - $pos);
            });
        }

        return min($positions);
    }

    private function partTwo(): int
    {
        $positions = [];

        for ($i = $this->min(); $i <= $this->max(); $i++) {
            if (! array_key_exists($i, $positions)) {
                $positions[$i] = 0;
            }

            $this->getInput()->each(function ($pos) use (&$positions, $i) {
                $diff = abs($i - $pos);

                $amount = 0;
                for ($j = 0; $j < $diff; $j++) {
                    $amount += 1 + $j;
                }

                $positions[$i] += $amount;
            });
        }

        return min($positions);
    }

    public function getFilepath(): string
    {
        return base_path('assets' . DIRECTORY_SEPARATOR . 'day-seven' . DIRECTORY_SEPARATOR . 'input.txt');
    }

    public function getInput(): Collection
    {
        return collect(explode(',', $this->readsInputGetInput()->first()))->transform(fn ($val) => (int)$val);
    }

    private function min(): int
    {
        return $this->getInput()->min();
    }

    private function max(): int
    {
        return $this->getInput()->max();
    }
}
