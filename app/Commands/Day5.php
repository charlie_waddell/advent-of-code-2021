<?php

namespace App\Commands;

use App\Traits\ReadsInput;
use Illuminate\Support\Collection;
use LaravelZero\Framework\Commands\Command;

class Day5 extends Command implements Contracts\ReadsInput
{
    use ReadsInput{
        getInput as readsInputGetInput;
    }

    protected $signature = 'day:five';
    protected $description = 'Day five of advent of code';

    private array $map = [];

    public function handle(): void
    {
        $this->initMap();
        $this->partOne();
        $this->partTwo();
    }

    private function partOne()
    {
        $this->horizontalAndVerticalVents()->each(function (Collection $coordinates) {
            $this->markLine($coordinates->get('start'), $coordinates->get('end'));
        });

        $this->info("Part one: " . $this->getNumberOfIntersections());
    }

    private function partTwo()
    {
        $this->diagonalVents()->each(function (Collection $coordinates) {
            $this->markLine($coordinates->get('start'), $coordinates->get('end'));
        });

        $this->info("Part two: " . $this->getNumberOfIntersections());
    }

    private function getNumberOfIntersections(): int
    {
        return collect($this->map)->flatten()->filter(fn ($point) => $point > 1)->count();
    }

    private function horizontalAndVerticalVents(): Collection
    {
        return $this->getInput()->filter(function (Collection $coordinates) {
            return ! $this->ventIsDiagonal($coordinates->get('start'), $coordinates->get('end'));
        })->values();
    }

    private function diagonalVents(): Collection
    {
        return $this->getInput()->filter(function (Collection $coordinates) {
            return $this->ventIsDiagonal($coordinates->get('start'), $coordinates->get('end'));
        })->values();
    }

    private function markLine(Collection $from, Collection $to): void
    {
        $xValues = collect(range($from->get('x'), $to->get('x')));
        $yValues = collect(range($from->get('y'), $to->get('y')));

        if ($this->ventIsDiagonal($from, $to)) {
            $xValues->each(fn ($x, $index) => $this->map[$yValues[$index]][$x]++);
        } else {
            $xValues->each(fn ($x) => $yValues->each(fn ($y) => $this->map[$y][$x]++));
        }
    }

    private function ventIsDiagonal(Collection $from, Collection $to): bool
    {
        return $from->get('x') !== $to->get('x') && $from->get('y') !== $to->get('y');
    }

    private function initMap()
    {
        for ($i = 0; $i <= $this->getMaxY(); $i++) {
            $this->map[] = array_fill(0, $this->getMaxX() + 1, 0);
        }
    }

    private function getMaxX(): int
    {
        return $this->getInput()->pluck('start')->pluck('x')->merge($this->getInput()->pluck('end')->pluck('x'))->max();
    }

    private function getMaxY(): int
    {
        return $this->getInput()->pluck('start')->pluck('y')->merge($this->getInput()->pluck('end')->pluck('y'))->max();
    }

    public function getInput(): Collection
    {
        if ($this->lines) {
            return $this->lines;
        }

        return $this->lines = $this->readsInputGetInput()->map(function (string $coordinates) {
            [$start, $end] = explode(' -> ', $coordinates);
            [$startX, $startY] = explode(',', $start);
            [$endX, $endY] = explode(',', $end);

            return collect([
                'start' => collect([
                    'x' => (int) $startX,
                    'y' => (int) $startY
                ]),
                'end' => collect([
                    'x' => (int) $endX,
                    'y' => (int) $endY
                ])
            ]);
        });
    }

    /**
     * @return string
     */
    public function getFilepath(): string
    {
        return base_path('assets' . DIRECTORY_SEPARATOR . 'day-five' . DIRECTORY_SEPARATOR . 'input.txt');
    }
}
