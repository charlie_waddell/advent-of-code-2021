<?php

namespace App\Commands;

use App\Traits\ReadsInput;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use LaravelZero\Framework\Commands\Command;

class Day3 extends Command implements Contracts\ReadsInput
{
    use ReadsInput;

    private const OXYGEN = 1;
    private const CO2 = 2;

    protected $signature = 'day:three';
    protected $description = 'Day three of advent of code';

    public function handle(): void
    {
        $this->info("Part one: {$this->partOne()}");
        $this->info("Part two: {$this->partTwo()}");
    }

    private function partOne(): int
    {
        $tallies = $this->getTallies($this->getInput());

        $gammaRate = '';
        $epsilonRate = '';

        foreach ($tallies as $tally) {
            $max = max($tally);
            $min = min($tally);

            $tally = array_flip($tally);

            $gammaRate .= $tally[$max];
            $epsilonRate .= $tally[$min];
        }

        return bindec($gammaRate) * bindec($epsilonRate);
    }

    private function partTwo(): int
    {
        return $this->getGeneratorRating(static::OXYGEN) * $this->getGeneratorRating(static::CO2);
    }

    private function getTallies(Collection $input): array
    {
        $tallies = [];
        $input->each(function (string $line) use (&$tallies) {
            collect(str_split($line))->each(function ($binaryDigit, $key) use (&$tallies) {
                if (! empty($tallies[$key][$binaryDigit])) {
                    $tallies[$key][$binaryDigit]++;
                } else {
                    $tallies[$key][$binaryDigit] = 1;
                }
            });
        });

        return $tallies;
    }

    private function getGeneratorRating(int $type): int
    {
        $input = $this->getInput();
        $pos = 0;

        do {
            $tally = Arr::get($this->getTallies($input), $pos);
            $min = min($tally);
            $max = max($tally);
            $digit = $min === $max
                ? ($type === static::OXYGEN ? 1 : 0)
                : (int) array_flip($tally)[($type === static::OXYGEN ? $max : $min)];

            $input = $input->filter(function ($value) use ($digit, $pos) {
                return $digit === (int) substr($value, $pos, 1);
            });
            $pos++;
        } while ($input->count() > 1);

        return bindec($input->first());
    }

    /**
     * @return string
     */
    public function getFilepath(): string
    {
        return base_path('assets' . DIRECTORY_SEPARATOR . 'day-three' . DIRECTORY_SEPARATOR . 'input.txt');
    }
}
