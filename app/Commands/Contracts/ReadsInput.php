<?php

namespace App\Commands\Contracts;

use Illuminate\Support\Collection;

interface ReadsInput
{
    public function getInput(): Collection;
    public function getFilepath(): string;
}
