<?php

namespace App\Commands;

use App\Traits\ReadsInput;
use LaravelZero\Framework\Commands\Command;

class Day2 extends Command implements Contracts\ReadsInput
{
    use ReadsInput;

    protected $signature = 'day:two';
    protected $description = 'Day two of advent of code';

    private int $horizontal = 0;
    private int $depth = 0;
    private int $depthWithAim = 0;

    public function handle(): void
    {
        $this->getInput()->each(function (string $input) {
            [$instruction, $value] = explode(' ', $input);
            $this->{$instruction}($value);
        });

        $this->info('Part one: ' . $this->horizontal * $this->depth);
        $this->info('Part two: ' . $this->horizontal * $this->depthWithAim);
    }

    private function forward(int $value): void
    {
        $this->horizontal += $value;
        $this->depthWithAim += ($value * $this->depth);
    }

    private function up(int $value): void
    {
        $this->down($value * -1);
    }

    private function down(int $value): void
    {
        $this->depth += $value;
    }

    public function getFilepath(): string
    {
        return base_path('assets' . DIRECTORY_SEPARATOR . 'day-two' . DIRECTORY_SEPARATOR . 'input.txt');
    }
}
