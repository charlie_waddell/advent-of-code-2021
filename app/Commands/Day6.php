<?php

namespace App\Commands;

use App\Traits\ReadsInput;
use Illuminate\Support\Collection;
use LaravelZero\Framework\Commands\Command;

class Day6 extends Command implements Contracts\ReadsInput
{
    use ReadsInput{
        getInput as readsInputGetInput;
    }

    protected $signature = 'day:six';
    protected $description = 'Day six of advent of code';

    private int $aboutToGiveBirth = 0;
    private int $recentlyBecameAParent = 6;
    private int $newBornFish = 8;

    public function handle(): void
    {
        $this->info("Part one: " . $this->getFishPopulationAfterXDays(80));
        $this->info("Part two: " . $this->getFishPopulationAfterXDays(256));
    }

    private function getFishPopulationAfterXDays(int $days): float
    {
        $fishCounts = $this->calculateInitialFishCounts();

        for ($i = 0; $i < $days; $i++) {
           $newFishCounts = $this->calculateNewFishCountsFromPrevious($fishCounts);
           $fishCounts = $newFishCounts;
       }

        return array_sum($fishCounts);
    }

    private function calculateInitialFishCounts(): array
    {
        $counts = [];

        for ($i = $this->aboutToGiveBirth; $i <= $this->newBornFish; $i++) {
            $counts[$i] = $this->getInput()->filter(fn ($count) => $count === $i)->count();
        }

        return $counts;
    }

    private function calculateNewFishCountsFromPrevious(array $fishCounts)
    {
        $tmpFishCounts = $fishCounts;

        for ($i = $this->aboutToGiveBirth; $i <= $this->newBornFish; $i++) {
            if ($i === $this->aboutToGiveBirth) {
                $fishCounts[$this->recentlyBecameAParent] += $tmpFishCounts[$i];
                $fishCounts[$this->newBornFish] += $tmpFishCounts[$i];
            } else {
                $fishCounts[$i - 1] += $tmpFishCounts[$i];
            }
            $fishCounts[$i] -= $tmpFishCounts[$i];
        }

        return $fishCounts;
    }

    public function getInput(): Collection
    {
        return collect(explode(',', $this->readsInputGetInput()->first()))->map(fn ($value) => (int) $value);
    }

    /**
     * @return string
     */
    public function getFilepath(): string
    {
        return base_path('assets' . DIRECTORY_SEPARATOR . 'day-six' . DIRECTORY_SEPARATOR . 'input.txt');
    }
}
