<?php

namespace App\Commands;

use App\Traits\ReadsInput;
use LaravelZero\Framework\Commands\Command;

class Day1 extends Command implements Contracts\ReadsInput
{
    use ReadsInput;

    protected $signature = 'day:one';
    protected $description = 'Day one of advent of code';

    public function handle(): void
    {
        $this->info("Part two answer: {$this->partTwo()}");
        $this->info("Part one answer: {$this->partOne()}");
    }

    private function partOne(): int
    {
        $count = 0;

        foreach ($this->getInput() as $value) {
            if (isset($previous)) {
                $count += $previous < $value ? 1 : 0;
            }
            $previous = $value;
        }

        return $count;
    }

    private function partTwo(): int
    {
        $count = 0;
        $offset = 0;

        do {
            $groupOne = $this->getInput()->skip($offset)->take(3);
            $groupTwo = $this->getInput()->skip($offset + 1)->take(3);
            $count += $groupOne->sum() < $groupTwo->sum() ? 1 : 0;
            $offset++;
        } while ($groupOne->isNotEmpty());

        return $count;
    }

    /**
     * @return string
     */
    public function getFilepath(): string
    {
        return base_path('assets' . DIRECTORY_SEPARATOR . 'day-one' . DIRECTORY_SEPARATOR . 'input.txt');
    }
}
