<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;

trait ReadsInput
{
    private ?Collection $lines = null;

    public function getInput(): Collection
    {
        if ($this->lines) {
            return $this->lines;
        }

        $file = File::get($this->getFilepath());

        return $this->lines = collect(array_filter(explode("\n", $file)));
    }
}
